Code.require_file "../test_helper.exs", __ENV__.file
defmodule Ucx.TestAlarmManager do
  use ExUnit.Case
  alias Ucx.AlarmManager, as: AM

  setup do
    AM.start_link
    AM.clear_all
  end

  test "gets empty list" do
    assert AM.get == []
  end
end
