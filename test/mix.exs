defmodule UcxAlarmManager.Mixfile do
  use Mix.Project

  def project do
    [app: :ucx_alarm_manager,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger],
     mod: {Ucx.AlarmManager.Supervisor, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:exactor, ">= 2.1.2", override: true},
      {:ucx_license_manager,
       git: "git@bitbucket.org:emetrotel/ucx_license_manager.git", env: Mix.env},
    ]
  end
end
