defmodule Ucx.AlarmManager do
  @moduledoc """
  AlarmManager package for UCx applications

  Add `use Ucx.AlarmManager, opts \\ []` to your User module to add a number of
  Module functions and helpers.

  The optional `opts` parameter is used to pass different parameters.

  The application alarm manager provides the alarm handler functions.

  Alarms are either set or cleared.

  Alarms can be queried in the console with the alarms command. Each active
  alarm is listed with the name of the alarm and a brief description.

  """
  @event_collector_program   "evtsend"
  @critical                  1
  @major                     2
  @warning                   3
  @info                      4
  @event_send_fail           255
  @default_timer             1000
  @default_event_id_base     300
  @default_module            "Mdse"
  @default_module_name       Mdse.MdseAlarmManager

  use ExActor.GenServer, export: :ucx_alarm_manager
  require Logger

  defmacro __using__(opts \\ []) do
    quote bind_quoted: [opts: opts] do

      def start_link do
        Ucx.AlarmManager.start_link
      end

      def set(alarm, name, description) do
        Ucx.AlarmManager.set(alarm, name, description)
      end

      def set(alarm, id, name, description) do
        Ucx.AlarmManager.set(alarm, id, name, description)
      end

      def set(alarm, id, name, description, opts) do
        Ucx.AlarmManager.set(alarm, id, name, description, opts)
      end

      def set_no_clear(alarm, id, name, description, opts) do
        Ucx.AlarmManager.set_no_clear(alarm, id, name, description, opts)
      end

      def clear(alarm) do
        Ucx.AlarmManager.clear(alarm)
      end

      def clear(alarm, id) do
        Ucx.AlarmManager.clear(alarm, id)
      end

      def clear(alarm, id, opts) do
        Ucx.AlarmManager.clear(alarm, id, opts)
      end

      def clear_all do
        Ucx.AlarmManager.clear_all
      end

      def print do
        Ucx.AlarmManager.print
      end

      def alarms do
        Ucx.AlarmManager.alarms
      end

      def get do
        Ucx.AlarmManager.get
      end

      def get_state do
        Ucx.AlarmManager.get_state
      end

      defoverridable [start_link: 0, set: 3, set: 4, set: 5, clear: 1, clear: 2, clear: 3]

    end
  end

  @doc """
  start the process
  """
  defstart start_link do
    alarm_list = Application.get_env :ucx_alarm_manager, :alarm_list, []
    timer = get_timer()
    len = Enum.count(alarm_list)
    interval = div(timer, len)
    Enum.each(1..len, &(:erlang.start_timer(&1 * interval, self, Enum.at(alarm_list, &1 - 1))))
    initial_state []
  end

  @doc """
  Set a multiple type alarm

  Multiple type alarms are alarms that can have multiple alarms for a single type
  where each alarm is identified by an id. This is the type of alarm used for unsupported
  devices

  ## Example
      Ucx.AlarmManager.set(:unsupported_device, 10, "Unsupported Device", "Device has been disabled")
  """
  defcast set(alarm, id, name, description), state: state do
    set_alarm(state, alarm, id, name, description)
      |> new_state
  end

  defcast set(alarm, id, name, description, opts), state: state do
    set_alarm(state, alarm, id, name, description, opts)
      |> new_state
  end

  @doc """
  Set a single type alarm

  Single type alarms can only have one instance.

  """
  defcast set(alarm, name, description), state: state do
    set_alarm(state, alarm, name, description)
      |> new_state
  end

  @doc """
  Clear a multiple type alarm

  ## Example
      Ucx.AlarmManager.clear(:unsupported_device, 10)
  """
  defcast clear(alarm, id), state: state do
    clear_alarm(state, alarm, id)
      |> new_state
  end

  defcast clear(alarm, id, opts), state: state do
    clear_alarm(state, alarm, id, opts)
      |> new_state
  end

  @doc """
  Clear a single type alarm

  """
  defcast clear(alarm), state: state do
    clear_alarm(state, alarm)
      |> new_state
  end

  @doc """
  Raise a multiple type alarm, which does not have a clear event.

  """
  defcast Ucx.AlarmManager.set_no_clear(alarm, id, name, description, opts), state: state do
    event_collector_set_no_clear_alarm(description, opts)
    noreply, state
  end

  @doc """
  Print all active alarms
  """
  defcast print, state: state do
    get_alarms(state)
      |> Enum.each(&(IO.puts &1))
    noreply
  end

  @doc """
  Clear all alarms

  Used mostly for testing
  """
  defcast clear_all do
    new_state []
  end

  @doc """
  Returns a string of the alarms
  """
  defcall alarms, state: state do
    get_alarms(state)
    |> Enum.map(&(&1))
    |> reply
  end

  @doc """
  Returns current alarm list of strings
  """
  defcall get, state: state do
    get_alarms(state)
      |> reply
  end

  @doc """
  Returns the current AlarmManager state

  Used mostly for testing
  """
  defcall get_state, state: state, do: reply(state)

  def handle_info({:timeout, _ref, data}, state) do
    timer = get_timer()
    :erlang.start_timer(timer, self, data)
    new_state handle_alarm(state, data)
  end

  ################
  # Alarm Handlers

  # Alarm handlers are defined in application module

  ###########
  # Helpers

  @doc false
  def get_alarms(state) do
    for {_key, value} <- state do
      case value do
        {name, description} ->
          "#{name}: #{description}"
        hash ->
          get_alarms Map.to_list(hash)
      end
    end
    |> List.flatten
    |> Enum.sort
  end

  @doc false
  def handle_alarm(state, data) do
    case data do
      {alarm, {:single, name, description}} ->
        check_alarm(state, alarm, name, description)

      {alarm, {:multiple, _name, _description}} ->
        Keyword.get(state, alarm, %{})
        |> Enum.reduce(state, fn({id, {name, description}}, acc) ->
          check_alarm(acc, alarm, id, name, description)
        end)
    end
  end

  defp check_alarm(state, alarm, name, description) do
    module_name = get_module_name()
    if apply(module_name, alarm, []) do
      set_alarm(state, alarm, name, description)
    else
      clear_alarm(state, alarm)
    end
  end

  defp check_alarm(state, alarm, id, name, description) do
    module_name = get_module_name()
    if apply(module_name, alarm, [id]) do
      set_alarm(state, alarm, id, name, description)
    else
      clear_alarm(state, alarm, id)
    end
  end

  defp set_alarm(state, alarm, name, description) do
    validate_alarm alarm
    if name do
      Keyword.put state, alarm, {name, description}
    else
      state
    end
  end

  @doc false
  def set_alarm(state, alarm, id, name, description, opts \\ []) do
    validate_alarm alarm
    if name do
      list =
        state
        |> Keyword.get(alarm, %{})
        |> Map.put(id, {name, description})
      state
      |> Keyword.put(alarm, list)
      |> handle_addons(description, opts)
    else
      state
    end
  end

  defp handle_addons(state, _, []), do: state
  defp handle_addons(state, description, [h|t]) do
    state
    |> handle_addon(description, h)
    |> handle_addon(description, t)
  end

  defp handle_addon(state, _description, []), do: state
  defp handle_addon(state, description, {:ucx_event, opts}) do
    event_collector_set_alarm(description, opts)
    state
  end

  defp handle_addon(_state, description, opt) do
    raise "unknown set alarm: description #{description} opt #{inspect opt}"
  end

  defp event_collector_set_alarm(description, opts) do
    [event_id, severity, action, acknowledge] = opts
    raise_clear_alarm(event_id, severity, description, action, acknowledge)
  end

  defp event_collector_clear_alarm(opts) do
    [event_id, severity, description, acknowledge] = opts
    raise_clear_alarm(event_id, severity, description, "", acknowledge)
  end

  defp event_collector_set_no_clear_alarm(description, opts) do
    [event_id, severity, action, acknowledge] = opts
    raise_clear_alarm(event_id, severity, description, action, acknowledge)
  end

  defp clear_alarm(state, alarm, id, opts \\ []) do
    validate_alarm alarm
    state
    |> handle_clear_alarm(alarm, id)
    |> handle_addons(opts)
  end

  defp clear_alarm(state, alarm) do
    validate_alarm alarm
    Keyword.delete state, alarm
  end


  defp handle_addons(state, []), do: state
  defp handle_addons(state, [h|t]) do
    state
    |> handle_addon(h)
    |> handle_addon(t)
  end

  defp handle_addon(state, []), do: state
  defp handle_addon(state, {:ucx_event, opts}) do
    event_collector_clear_alarm(opts)
    state
  end

  defp handle_addon(_state, opt) do
    raise "unknown clear alarm: opt #{inspect opt}"
  end

  defp handle_clear_alarm(state, alarm, id) do
    case Keyword.get(state, alarm) do
      nil -> state
      map ->
        case Map.delete(map, id) do
          map when map == %{} -> Keyword.delete(state, alarm)
          map                 -> Keyword.put(state, alarm, map)
        end
    end
  end

  defp validate_alarm(alarm) do
    alarm_list = get_alarm_list()
    unless Keyword.get(alarm_list, alarm), do: raise("Invalid Alarm")
  end

  def raise_clear_alarm(event_id, severity, description, action, acknowledge) do
    case System.find_executable(@event_collector_program) do
      nil ->
         Logger.error "Program " <> @event_collector_program <> " is not available"
         @event_send_fail
      path ->
        module = get_module()
        {_, result} = System.cmd(path, [module, to_string(get_event_id_base + event_id),
                                 to_string(get_severity(severity)), description, action,
                                 to_string(get_acknowledge(acknowledge))])
        if (result != 0), do:
          Logger.error "Failed to send alarm to event collector: event id #{event_id} description #{description}"
        result
    end
  end

  defp get_event_id_base do
    Application.get_env :ucx_alarm_manager, :event_id_base, @default_event_id_base
  end

  defp get_severity(severity) do
    case severity do
      :critical -> @critical
      :major    -> @major
      :warning  -> @warning
      :info     -> @info
      _         -> @info
    end
  end

  defp get_acknowledge(ack) do
    if ack == :acknowledge_yes, do: 1, else: 0
  end

  defp get_timer() do
    Application.get_env :ucx_alarm_manager, :timer, @default_timer
  end

  defp get_module() do
    Application.get_env :ucx_alarm_manager, :module, @default_module
  end

  defp get_module_name() do
    Application.get_env :ucx_alarm_manager, :module_name, @default_module_name
  end

  defp get_alarm_list() do
    Application.get_env :ucx_alarm_manager, :alarm_list, []
  end

end

