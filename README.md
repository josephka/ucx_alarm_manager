# UcxAlarmManager

AlarmManager package for UCx applications

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `ucx_alarm_manager` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ucx_alarm_manager, "~> 0.1.0"}]
    end
    ```

  2. Ensure `ucx_alarm_manager` is started before your application:

    ```elixir
    def application do
      [applications: [:ucx_alarm_manager]]
    end
    ```

